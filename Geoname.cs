﻿using System;

public class Rootobject
{
    public Geoname[] geonames { get; set; }
}

public class Geoname
{
    public string summary { get; set; }
    public int elevation { get; set; }
    public int geoNameId { get; set; }
    public string feature { get; set; }
    public float lng { get; set; }
    public string countryCode { get; set; }
    public int rank { get; set; }
    public string lang { get; set; }
    public string title { get; set; }
    public float lat { get; set; }
    public string wikipediaUrl { get; set; }
}
