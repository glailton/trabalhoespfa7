﻿using System;
namespace TrabalhoEspFa7.Droid
{
    public class Rootobject
    {
        public Geoname[] geonames { get; set; }
    }

    public class Geoname
    {
        public string adminCode1 { get; set; }
        public string lng { get; set; }
        public int geoNameId { get; set; }
        public string toponymName { get; set; }
        public string countryId { get; set; }
        public string fcl { get; set; }
        public Int64 population { get; set; }
        public string countryCode { get; set; }
        public string name { get; set; }
        public string fclName { get; set; }
        public string countryName { get; set; }
        public string fcodeName { get; set; }
        public string adminName1 { get; set; }
        public string lat { get; set; }
        public string fcode { get; set; }
    }
}