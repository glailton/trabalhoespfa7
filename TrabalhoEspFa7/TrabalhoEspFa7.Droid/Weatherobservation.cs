﻿using System;

namespace TrabalhoEspFa7.Droid
{
    public class RootobjectWeather
    {
        public Weatherobservation weatherObservation { get; set; }
    }

    public class Weatherobservation
    {
        public int elevation { get; set; }
        public float lng { get; set; }
        public string observation { get; set; }
        public string ICAO { get; set; }
        public string clouds { get; set; }
        public string dewPoint { get; set; }
        public string datetime { get; set; }
        public float seaLevelPressure { get; set; }
        public string countryCode { get; set; }
        public string temperature { get; set; }
        public int humidity { get; set; }
        public string stationName { get; set; }
        public string weatherCondition { get; set; }
        public string windSpeed { get; set; }
        public float lat { get; set; }
    }
}