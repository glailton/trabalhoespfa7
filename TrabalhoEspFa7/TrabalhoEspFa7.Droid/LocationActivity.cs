using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content;

namespace TrabalhoEspFa7.Droid
{
    [Activity(Label = "TrabalhoEspFa7", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.KeyboardHidden)]
    public class LocationActivity : Activity
    {
        private Spinner spinnerCountry;
        private Button btnAddCity;
        private EditText edtCityName;
        private ListView listCity;

        private List<string> cities = new List<string>();
        private List<string> countries = new List<string>();

        private Geoname geonames;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.list_location);

            geonames = new Geoname();

            spinnerCountry = FindViewById<Spinner>(Resource.Id.spinnerCountry);
            btnAddCity = FindViewById<Button>(Resource.Id.addButton);
            edtCityName = FindViewById<EditText>(Resource.Id.cityText);
            listCity = FindViewById<ListView>(Resource.Id.listView);

            btnAddCity.Click += Button_Click;
            listCity.ItemClick += ListCity_ItemClick;

            spinnerCountry.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                this, Resource.Array.country_array, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinnerCountry.Adapter = adapter;
        }

        private void ListCity_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var listView = sender as ListView;
            char[] delimiterChars = { '-' };
            var l = listCity.GetItemAtPosition(e.Position).ToString();
            string[] res = l.Split(delimiterChars);
            var activity = new Intent(this, typeof(MainActivity));
            Toast.MakeText(this, res[0], ToastLength.Long).Show();
            activity.PutExtra("country", geonames.countryName);
            activity.PutExtra("city", res[0]);
            StartActivity(activity); 
        }

        private async void Button_Click(object sender, EventArgs e)
        {
            geonames.toponymName = edtCityName.Text;
            cities.Add(geonames.toponymName + " - " + geonames.countryName);
            countries.Add(geonames.countryName);

            ArrayAdapter<string> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, cities);
            listCity.Adapter = adapter;

            edtCityName.Text = "";
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            geonames.countryName = spinner.GetItemAtPosition(e.Position).ToString();
        }

    }
}