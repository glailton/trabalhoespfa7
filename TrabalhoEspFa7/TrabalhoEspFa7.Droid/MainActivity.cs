﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading.Tasks;
using System.Json;
using System.Net;
using System.IO;
using Android.Util;

namespace TrabalhoEspFa7.Droid
{
    [Activity(Label = "TrabalhoEspFa7", Icon = "@drawable/icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : Activity
    {
        private TextView cityName;
        private TextView stateName;
        private TextView countryName;
        private TextView lat;
        private TextView lng;
        private TextView obs;
        private TextView temperature;
        private TextView datetime;
        private TextView humidity;
        private TextView conditions;
        private TextView population;
        private Button button;

        private Geoname geonames;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Main);

            geonames = new Geoname();

            cityName = FindViewById<TextView>(Resource.Id.toponymName);
            stateName = FindViewById<TextView>(Resource.Id.adminName1);
            countryName = FindViewById<TextView>(Resource.Id.countryName);
            lat = FindViewById<TextView>(Resource.Id.lat);
            lng = FindViewById<TextView>(Resource.Id.lng);
            temperature = FindViewById<TextView>(Resource.Id.temperature);
            datetime = FindViewById<TextView>(Resource.Id.datetime);
            humidity = FindViewById<TextView>(Resource.Id.humidity);
            conditions = FindViewById<TextView>(Resource.Id.conditions);
            population = FindViewById<TextView>(Resource.Id.population);
            button = FindViewById<Button>(Resource.Id.ButtonBack);

            button.Click += Button_Click;

            StartFetchData();
        }

        private void Button_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private async void StartFetchData()
        {
            geonames.countryName = Intent.GetStringExtra("country") ?? "Data not avaliable";
            geonames.toponymName = Intent.GetStringExtra("city") ?? "Data not avaliable";
            Console.Out.WriteLine("Pais: {0}", geonames.countryName);
            Console.Out.WriteLine("cidade: {0}", geonames.toponymName);

            Toast.MakeText(this, "Pais: " + geonames.countryName + " City: " + geonames.toponymName, ToastLength.Long).Show();

            string urlCityName = "http://api.geonames.org/searchJSON?name_startsWith=" + geonames.toponymName.Trim() + "&maxRows=10&lang=pt&username=glailton";
            Console.Out.WriteLine("URL: {0}", urlCityName);
            JsonValue jsonCityName = await FetchWeatherAsync(urlCityName);

            JsonValue jsonLatLng = await FetchWeatherAsync(GetLongLat(jsonCityName));
            ParseAndDisplay(jsonLatLng);

        }

        private async Task<JsonValue> FetchWeatherAsync(string url)
        {
            Console.Out.WriteLine("URL: {0}", url);
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    // Return the JSON document:
                    return jsonDoc;
                }
            }
        }

        private string GetLongLat(JsonValue json)
        {
            JsonValue geoNamesValue = json["geonames"];
            Console.Out.WriteLine("Tamanho: {0}", geoNamesValue.Count);
            string urlLatLng = "";

            for (int i = 0; i < geoNamesValue.Count; i++)
            {
                JsonValue geoNamesResults = json["geonames"][i];
                Console.Out.WriteLine("Country: {0}", geoNamesResults["countryName"].ToString());
                Console.Out.WriteLine("Lat: {0}", geoNamesResults["lat"].ToString());
                string countryName = geoNamesResults["countryName"];
               // Console.Out.WriteLine("Country name: {0}", countryName.Equals("United States"));

                //  geonames.lat = geoNamesResultsA["lat"].ToString();
                //  Console.Out.WriteLine("Lat: {0}", geonames.lat);
                if (countryName.Equals(geonames.countryName))
                {
                    
                    geonames.lat = geoNamesResults["lat"];
                    Console.Out.WriteLine("Lat_dentro: {0}", geonames.lat);
                    geonames.lng = geoNamesResults["lng"];
                    Console.Out.WriteLine("Lng_dentro: {0}", geonames.lng);
                    geonames.population = geoNamesResults["population"];
                    Console.Out.WriteLine("pop: {0}", geonames.population);
                    geonames.adminName1 = geoNamesResults["adminName1"];
                    geonames.toponymName = geoNamesResults["toponymName"];
                    geonames.countryName = geoNamesResults["countryName"];
                    //Console.Out.WriteLine("Teste: {0}", geoNamesResults.ToString());
 
                    urlLatLng = "http://api.geonames.org/findNearByWeatherJSON?lat=" +
                                       geonames.lat +
                                       "&lng=" +
                                       geonames.lng +
                                       "&lang=pt&username=glailton";
                    Console.Out.WriteLine("URL_1: {0}", urlLatLng);
                    break;
                }
            }
            return urlLatLng;
        }

        private void ParseAndDisplay(JsonValue json)
        {
            JsonValue jsonResults = json["weatherObservation"];

            cityName.Text = geonames.toponymName;
            stateName.Text = geonames.adminName1;
            countryName.Text = geonames.countryName;
            lng.Text = geonames.lng;
            lat.Text = geonames.lat;
            population.Text = geonames.population.ToString();

            string temp = jsonResults["temperature"];
            temperature.Text = String.Format("{0:F1}", temp) + "° C";
            datetime.Text = jsonResults["datetime"];

            int humidPercent = jsonResults["humidity"];
            humidity.Text = humidPercent + "%";

            // Get the "clouds" and "weatherConditions" strings and combine them.
            // Ignore strings that are reported as "n/a":
            string cloudy = jsonResults["clouds"];
            if (cloudy.Equals("n/a"))
                cloudy = "";
            string cond = jsonResults["weatherCondition"];
            if (cond.Equals("n/a"))
                cond = "";

            // Write the result to the conditions TextBox:
            conditions.Text = cloudy + " " + cond;
        }
    }
}

